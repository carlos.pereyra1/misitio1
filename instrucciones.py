#Para poder migrar primero makemigration y luego migrate

"""
EJEMPLO:
python manage.py makemigrations blog
python manage.py migrate blog
"""
#En la carpeta urls van a estar todas las url que se quiera mostrar

"""
Anotaciones:
Recordar que para cada nueva aplicacion se debe poner:

from django.urls import path #SIEMPRE

from . import views #Para poder ejecutar las vistas las secciones

urlpatterns = [
    path('', views.hola),#se deja en blanco la pagina principal
    path('about/', views.about),#se deja en blanco la pagina principal
   
]

# EN LA APLICACION PRINCIPAL EN ESTE CASO MISITIO1 poner el include

from django.urls import path, include e INCLUIR LA APP en este caso 'blog.urls'

urlpatterns = [
    path('', include('blog.urls')),
]

INSTALAR DB BROWSER FOR SQLITE PARA VER LA BDD
"""
